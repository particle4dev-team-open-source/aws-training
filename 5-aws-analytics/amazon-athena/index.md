---
description: amazon athena
keywords: amazon athena, aws, analytics
title: amazon athena
---

##### [What are some good uses for Amazon Athena?](https://www.quora.com/What-are-some-good-uses-for-Amazon-Athena)

Just started playing with AWS Athena. It’s a very simple and convenient way to query data in an S3 bucket. To begin, go to your account settings and turn on Athena.

**[Edit - 2017–01–30 - Ashish pointed out that AWS docs say Athena is actually Presto - see his answer.]**

It looks to me like Athena is an abstraction of Hive: it allows you to create a table and populate it with data from an S3. AWS docs say, “Athena’s data catalog is Hive metastore compatible” and “The concept of SerDes in Athena is the same as the concept used in Hive.”

Ref.: [Amazon Athena FAQs – Amazon Web Services (AWS)](https://aws.amazon.com/athena/faqs/)

[SERDES: How does Hive stores data and what is SerDe?](http://stackoverflow.com/questions/14605640/how-does-hive-stores-data-and-what-is-serde)

It uses standard SQL (create table as select * from <bucket>… select colx, coly, colz from table…)

It costs (at the time of this writing) $5 per terabyte scanned in a query.

You don’t manage the hive infrastructure - all the nodes and other aspects are hidden. You just write standard SQL.

Given these factors, I can think of some use cases:

    You are testing a new data source. You want to do some basic data profiling to see if the data looks valid or if it’s garbage. You might want to run min/max date, rows per date, largest string in a particular varchar column, run some basic business queries to see if the data looks reasonable.

    You have data in S3 that will only need a relatively small number of queries to be run against it to satisfy the business.

    The business is in a hurry to get some reports on new data and there is not enough time to set up a conventional data platform. In this case, cost may be less of a factor than expediency.

#### [Why is Amazon Athena so fast?](https://www.quora.com/Why-is-Amazon-Athena-so-fast)

As Robin stated, speed is relative and a function of your personal perception : )

Athena is based on Facebook Presto ([Presto | Distributed SQL Query Engine for Big Data](https://prestodb.io/)). I assume that Amazon has made optimizations to Presto for their environment with AWS Athena.

One of the critical considerations for Athena is the object format on S3. If you are scanning a 1 TB CSV file your performance will NOT be the same as if the data was in a Parquet formatted file. Here is a comparison between the two:

I would imagine the demo you witnessed was leveraging an optimized file format like Parquet. Also, Athena would not be the first choice if speed is an absolute concern. However, since the the speed you witnessed was “fast”, using Athena might be the right fit. It can be a compelling proposition for a variety of use cases.

Here is some more thoughts on the topic:

[How to Be a Hero with Powerful Parquet, Google and Amazon](https://blog.openbridge.com/how-to-be-a-hero-with-powerful-parquet-google-and-amazon-f2ae0f35ee04)

If you are looking to get started with Athena, we just launched a 60 Second Setup, Zero Administration And Automatic Optimization product that wraps Athena in a lot of goodness:

[AWS Athena Automated — 60 Second Setup, Zero Administration And Automatic Optimization](https://blog.openbridge.com/aws-athena-automated-60-second-setup-zero-administration-and-automatic-optimization-eba474e9897a)

##### Resources
